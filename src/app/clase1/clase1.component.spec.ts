import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Clase1Component } from './clase1.component';

describe('Clase1Component', () => {
  let component: Clase1Component;
  let fixture: ComponentFixture<Clase1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Clase1Component ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Clase1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
