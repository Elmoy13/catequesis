import { Component, OnInit } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';

import { from } from 'rxjs';
@Component({
  selector: 'app-clase1',
  templateUrl: './clase1.component.html',
  styleUrls: ['./clase1.component.scss'],
})
export class Clase1Component implements OnInit {
  infiniteScroll: IonInfiniteScroll;
  data: any;
  arrayAct:any[]=[{"img":"../../assets/Imagenes/actividad1-1.PNG","titulo":"Tema 1","descripcion":"Celebracion de inicio"},
  {"img":"../../assets/Imagenes/actividad1-2.PNG","titulo":"UNO","descripcion":"hola"}
]
  constructor() {
    console.log(this.arrayAct)
   }

  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.data.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }

  toggleInfiniteScroll() {
    this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  }

  ngOnInit() {}

}
