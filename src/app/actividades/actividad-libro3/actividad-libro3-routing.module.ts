import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadLibro3Page } from './actividad-libro3.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadLibro3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadLibro3PageRoutingModule {}
