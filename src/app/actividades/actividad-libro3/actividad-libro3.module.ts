import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadLibro3PageRoutingModule } from './actividad-libro3-routing.module';

import { ActividadLibro3Page } from './actividad-libro3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadLibro3PageRoutingModule
  ],
  declarations: [ActividadLibro3Page]
})
export class ActividadLibro3PageModule {}
