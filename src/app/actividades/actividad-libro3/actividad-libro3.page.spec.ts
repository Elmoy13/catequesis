import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActividadLibro3Page } from './actividad-libro3.page';

describe('ActividadLibro3Page', () => {
  let component: ActividadLibro3Page;
  let fixture: ComponentFixture<ActividadLibro3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadLibro3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActividadLibro3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
