import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadesPageRoutingModule } from './actividades-routing.module';

import { ActividadesPage } from './actividades.page';

import { ModalActividadesPage } from 'src/app/modal/modal-actividades/modal-actividades.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadesPageRoutingModule
  ],
  declarations: [ActividadesPage, ModalActividadesPage],
  entryComponents: [ModalActividadesPage]
})
export class ActividadesPageModule {}
