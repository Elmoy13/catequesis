import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadLibro4PageRoutingModule } from './actividad-libro4-routing.module';

import { ActividadLibro4Page } from './actividad-libro4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadLibro4PageRoutingModule
  ],
  declarations: [ActividadLibro4Page]
})
export class ActividadLibro4PageModule {}
