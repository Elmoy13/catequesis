import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActividadLibro4Page } from './actividad-libro4.page';

describe('ActividadLibro4Page', () => {
  let component: ActividadLibro4Page;
  let fixture: ComponentFixture<ActividadLibro4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadLibro4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActividadLibro4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
