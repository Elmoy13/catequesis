import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadLibro4Page } from './actividad-libro4.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadLibro4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadLibro4PageRoutingModule {}
