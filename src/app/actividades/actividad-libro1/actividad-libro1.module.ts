import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadLibro1PageRoutingModule } from './actividad-libro1-routing.module';

import { ActividadLibro1Page } from './actividad-libro1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadLibro1PageRoutingModule
  ],
  declarations: [ActividadLibro1Page]
})
export class ActividadLibro1PageModule {}
