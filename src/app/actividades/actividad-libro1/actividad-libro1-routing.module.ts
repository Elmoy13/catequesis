import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadLibro1Page } from './actividad-libro1.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadLibro1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadLibro1PageRoutingModule {}
