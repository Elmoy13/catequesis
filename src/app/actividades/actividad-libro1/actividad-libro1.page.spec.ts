import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActividadLibro1Page } from './actividad-libro1.page';

describe('ActividadLibro1Page', () => {
  let component: ActividadLibro1Page;
  let fixture: ComponentFixture<ActividadLibro1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadLibro1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActividadLibro1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
