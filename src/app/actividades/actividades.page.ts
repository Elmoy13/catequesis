import { Component, OnInit } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { Camera } from '@ionic-native/camera/ngx';
import { ModalActividadesPage } from 'src/app/modal/modal-actividades/modal-actividades.page';
import { VideolabService } from 'src/app/servicios/videolab.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.page.html',
  styleUrls: ['./actividades.page.scss'],
})
export class ActividadesPage implements OnInit {
  data: any;

  constructor(public alertController: AlertController, private modalController: ModalController, private videolabServise: VideolabService, private camera:Camera, public location:Location) { }
  items = [];
  image:any;
  loadData(event) {
    setTimeout(() => {
      console.log('Done');
      event.target.complete();

      // App logic to determine if all data is loaded
      // and disable the infinite scroll
      if (this.data.length == 1000) {
        event.target.disabled = true;
      }
    }, 500);
  }
  UsarCamara(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;

    }).catch(error => {
      console.log(error);
    })
  }

  SelectGaleria(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;

    }).catch(error => {
      console.log(error);
    })
  }
 
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alerta',
      message: '<strong>¿Estas seguro que quieres enviar esta tarea?</strong>',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Okay',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }
  async video(){
      const modal = await this.modalController.create({
      component: ModalActividadesPage,
      cssClass: 'my-modal-css',
      componentProps: { passurl: 'https://www.youtube.com/embed/N_Uk5FPhq-A' }
   

      });
    
     return await modal.present();
    
    
  }


  ngOnInit() {
    this.items = this.videolabServise.getVideo();
    console.log(this.items)
     }

}
  
