import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActividadLibro2Page } from './actividad-libro2.page';

const routes: Routes = [
  {
    path: '',
    component: ActividadLibro2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActividadLibro2PageRoutingModule {}
