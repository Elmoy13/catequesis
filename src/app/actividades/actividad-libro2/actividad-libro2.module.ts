import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActividadLibro2PageRoutingModule } from './actividad-libro2-routing.module';

import { ActividadLibro2Page } from './actividad-libro2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActividadLibro2PageRoutingModule
  ],
  declarations: [ActividadLibro2Page]
})
export class ActividadLibro2PageModule {}
