import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ActividadLibro2Page } from './actividad-libro2.page';

describe('ActividadLibro2Page', () => {
  let component: ActividadLibro2Page;
  let fixture: ComponentFixture<ActividadLibro2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActividadLibro2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ActividadLibro2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
