import { TestBed } from '@angular/core/testing';

import { CatequistaServiceService } from './catequista-service.service';

describe('CatequistaServiceService', () => {
  let service: CatequistaServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CatequistaServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
