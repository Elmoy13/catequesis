import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlumnoServiceService {
  url: string;
  token: string;

  constructor(public httpclient: HttpClient) {
   
    this.url='https://cd21d1f271dc.ngrok.io/api/';
  }
  
  getTareas(id){

    return this.httpclient.get( this.url+ 'tarea/show/'+id)
  }
  
  
}
