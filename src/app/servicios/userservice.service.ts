import { Injectable, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment as env } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class UsersService {
   token;

  verificateUser  = new EventEmitter();
  url: any;

  constructor(public httpclient: HttpClient) { 
    this.token = window.localStorage.getItem('token')
    this.url='https://c483e896e781.ngrok.io/api/';
  }

  Create(dato) {

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      
    })
    
    return this.httpclient.post( this.url+'user/create', {
      "name": dato.name,
      "email": dato.email,
      "password": dato.password,
      "rol": dato.rol,
      "verified": dato.verified,
      "photograpy": dato.photograpy,
      "libro": dato.libro
  
    },{ 'headers': headers } );
  }

  updatePhoto(id,photo){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    })
    return this.httpclient.put( this.url+'user/register/photo/'+id,{"photograpy":photo}, { 'headers': headers })
  }



  getAlumnos(){
    
    return this.httpclient.get( this.url+ 'user/show/rol/alumno/1')
  }
  getCatequista(){
    
    return this.httpclient.get(  this.url+'user/show/rol/caticasta/0')
  }
  getAllUsers(){
    return this.httpclient.get( 'users/show' )
  }
  getUsersFalse(){
    let token = localStorage.getItem('token')
    console.log(token)

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.get( 'users/show/approve/0', { 'headers': headers })
  }
  getUsersTrue(){
    let token = localStorage.getItem('token')
    console.log(token)

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.get('users/show/approve/1', { 'headers': headers })
  }


  getTrueFalse(approve){
    let token = localStorage.getItem('token')
    console.log(token)

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.get( 'users/show_breeders/criador/'+approve, { 'headers': headers })
  }

  getTrue(){
    let token = localStorage.getItem('token')
    console.log(token)

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.get( 'users/show_breeders/criador/1', { 'headers': headers })
  }


  updateApprove(id){
    let token = localStorage.getItem('token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.put( 'users/update/approve/'+id, { 'headers': headers })
  }

  deleteUser(id){
    let token = localStorage.getItem('token')
    console.log(token)

    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    })
    return this.httpclient.delete( 'users/delete/'+id, { 'headers': headers })
  }


  updatePassword(id: number,password){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + this.token
    })
    return this.httpclient.put( 'users/update/password', 
    {
      "id": id,
      "password": password
  },{ 'headers': headers }
    )
  }


}