
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Auth/login/login.component';
import { Clase1Component } from './clase1/clase1.component';
import { RegistroCatequistasComponent } from './Auth/registro-catequistas/registro-catequistas.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {path: 'login', component: LoginComponent },
  {path: 'registro-catequistas', component: RegistroCatequistasComponent },
  
  {path: 'clase1', component: Clase1Component},
  {
    path: 'list-alumnos',
    loadChildren: () => import('./list/list-alumnos/list-alumnos.module').then( m => m.ListAlumnosPageModule)
  },
  {
    path: 'registro-alum',
    loadChildren: () => import('./Auth/registro-alum/registro-alum.module').then( m => m.RegistroAlumPageModule)
  },
  {
    path: 'list-catequista',
    loadChildren: () => import('./list/list-catequista/list-catequista.module').then( m => m.ListCatequistaPageModule)
  },
  {
    path: 'registro-catequita',
    loadChildren: () => import('./Auth/registro-catequita/registro-catequita.module').then( m => m.RegistroCatequitaPageModule)
  },


  {path: 'clase1', component: Clase1Component},
  {
    path: 'libro1',
    loadChildren: () => import('./libros/libro1/libro1.module').then( m => m.Libro1PageModule)
  },
  {
    path: 'libro2',
    loadChildren: () => import('./libros/libro2/libro2.module').then( m => m.Libro2PageModule)
  },
  {
    path: 'libro3',
    loadChildren: () => import('./libros/libro3/libro3.module').then( m => m.Libro3PageModule)
  },
  {
    path: 'libro4',
    loadChildren: () => import('./libros/libro4/libro4.module').then( m => m.Libro4PageModule)
  },
  {
    path: 'actividad-libro1',
    loadChildren: () => import('./actividades/actividad-libro1/actividad-libro1.module').then( m => m.ActividadLibro1PageModule)
  },
  {
    path: 'actividad-libro2',
    loadChildren: () => import('./actividades/actividad-libro2/actividad-libro2.module').then( m => m.ActividadLibro2PageModule)
  },
  {
    path: 'actividad-libro3',
    loadChildren: () => import('./actividades/actividad-libro3/actividad-libro3.module').then( m => m.ActividadLibro3PageModule)
  },
  {
    path: 'actividad-libro4',
    loadChildren: () => import('./actividades/actividad-libro4/actividad-libro4.module').then( m => m.ActividadLibro4PageModule)
  },
  {
    path: 'list-tareas',
    loadChildren: () => import('./list/list-tareas/list-tareas.module').then( m => m.ListTareasPageModule)
  },  {
    path: 'actividades',
    loadChildren: () => import('./actividades/actividades.module').then( m => m.ActividadesPageModule)
  }


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }




