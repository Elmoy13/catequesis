import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/servicios/userservice.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-list-catequista',
  templateUrl: './list-catequista.page.html',
  styleUrls: ['./list-catequista.page.scss'],
})
export class ListCatequistaPage implements OnInit {

  arrayAlumnos:Array<any>;
  constructor( private apiAlumnos:UsersService, public location:Location ) { }

  ngOnInit() {
    this.showAlumnos();
  }

  goBack(){
    this.location.back(); 
  }
  showAlumnos(){
    this.apiAlumnos.getAlumnos().subscribe((data:any)=>{
      this.arrayAlumnos=data
      console.log(this.arrayAlumnos)
    })
  }


}
