import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListCatequistaPage } from './list-catequista.page';

describe('ListCatequistaPage', () => {
  let component: ListCatequistaPage;
  let fixture: ComponentFixture<ListCatequistaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListCatequistaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListCatequistaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
