import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListCatequistaPage } from './list-catequista.page';

const routes: Routes = [
  {
    path: '',
    component: ListCatequistaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListCatequistaPageRoutingModule {}
