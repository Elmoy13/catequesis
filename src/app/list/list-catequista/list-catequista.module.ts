import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListCatequistaPageRoutingModule } from './list-catequista-routing.module';

import { ListCatequistaPage } from './list-catequista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListCatequistaPageRoutingModule
  ],
  declarations: [ListCatequistaPage]
})
export class ListCatequistaPageModule {}
