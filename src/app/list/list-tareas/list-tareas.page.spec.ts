import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListTareasPage } from './list-tareas.page';

describe('ListTareasPage', () => {
  let component: ListTareasPage;
  let fixture: ComponentFixture<ListTareasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTareasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListTareasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
