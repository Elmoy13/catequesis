import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalTareasPage } from 'src/app/modal/modal-tareas/modal-tareas.page';
import { AlumnoServiceService } from 'src/app/servicios/alumno-service.service';


@Component({
  selector: 'app-list-tareas',
  templateUrl: './list-tareas.page.html',
  styleUrls: ['./list-tareas.page.scss'],
})
export class ListTareasPage implements OnInit {
  arrayAlumnos;

  constructor(private modalCtrl:ModalController, private alumnos:AlumnoServiceService) { }

  ngOnInit() {
    this.showAlumnos();
  }
  showAlumnos(){
    this.alumnos.getTareas(1).subscribe((data:any)=> {
      this.arrayAlumnos = data
    })
  }
  async TareaModal(datos){

    const modal = await this.modalCtrl.create({
      component: ModalTareasPage,
      componentProps: {data: datos}

    });
    modal.onDidDismiss()
      .then((data) => {
        const user = data['data']; 
    });
    return await modal.present()
   }

}
