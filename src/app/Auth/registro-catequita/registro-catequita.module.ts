import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistroCatequitaPageRoutingModule } from './registro-catequita-routing.module';

import { RegistroCatequitaPage } from './registro-catequita.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroCatequitaPageRoutingModule
  ],
  declarations: [RegistroCatequitaPage]
})
export class RegistroCatequitaPageModule {}
