import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroCatequitaPage } from './registro-catequita.page';

const routes: Routes = [
  {
    path: '',
    component: RegistroCatequitaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistroCatequitaPageRoutingModule {}
