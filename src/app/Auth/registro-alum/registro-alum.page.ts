import { Component, OnInit } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Camera } from '@ionic-native/camera/ngx';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from 'src/app/servicios/userservice.service';



@Component({
  selector: 'app-registro-alum',
  templateUrl: './registro-alum.page.html',
  styleUrls: ['./registro-alum.page.scss'],
})
export class RegistroAlumPage implements OnInit {
  CreateForm:FormGroup;
  image:any;
  arrayAlumnos:Array<any>;
  selectedPhoto: Blob;
 

  constructor(private camera:Camera, public location:Location, private FBuilder: FormBuilder,private api:UsersService) { 
   
  }

  ngOnInit() {
    this.BuildForm();
  }
    

  private BuildForm() {
    this.CreateForm = this.FBuilder.group({
      
      name: ['', Validators.required],
      rol: ['alumno', Validators.required],
      email: ['', Validators.required],
      photograpy: ['', Validators.required,],
      password: ['', Validators.required,],
      verified: ['1', Validators.required,],
      libro: ['', Validators.required,],
     

    });
  }

  goBack(){
    this.location.back(); 
  }

  UsarCamara(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;
      this.CreateForm.controls['photograpy'].setValue(this.image);
     
     
       
   
    }).catch(error => {
      console.log(error);
    })
  }

  SelectGaleria(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;
      
      this.CreateForm.controls['photograpy'].setValue(this.image);
       
    
    }).catch(error => {
      console.log(error);
    })
  }

  

  send(){
    
    if(this.CreateForm.value){
      this.api.Create(this.CreateForm.value).subscribe((data:any)=>{
      
        console.log(data)
      })
    }else{

      console.log('no valido');

    }
     
   }
   


}
