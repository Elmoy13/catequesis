import { Component, OnInit } from '@angular/core';
import { Camera } from '@ionic-native/camera/ngx';
import { Location } from '@angular/common';



@Component({
  selector: 'app-registro-catequistas',
  templateUrl: './registro-catequistas.component.html', 
  styleUrls: ['./registro-catequistas.component.scss'],
})
export class RegistroCatequistasComponent implements OnInit {
  image:any;

  constructor(private camera:Camera, public location:Location) { }

  ngOnInit() {}
  
  goBack(){
    this.location.back(); 
  }

  UsarCamara(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.CAMERA,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;

    }).catch(error => {
      console.log(error);
    })
  }

  SelectGaleria(){
    this.camera.getPicture({
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      encodingType: this.camera.EncodingType.JPEG,
      targetHeight: 1024,
      targetWidth: 1024,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }).then(resultado => {
      this.image ="data:image/jpeg;base64," + resultado;

    }).catch(error => {
      console.log(error);
    })
  }

}
