import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UsersService } from 'src/app/servicios/userservice.service';


@Component({
  selector: 'app-modal-tareas',
  templateUrl: './modal-tareas.page.html',
  styleUrls: ['./modal-tareas.page.scss'],
})
export class ModalTareasPage implements OnInit {
  arrayAlumnos:Array<any>;
@Input("data") data;

  constructor(private modalCtrl:ModalController,  private apiAlumnos:UsersService) { }

  ngOnInit() {
  }
  closeModal() {
    this.modalCtrl.dismiss();
  }
  

}
