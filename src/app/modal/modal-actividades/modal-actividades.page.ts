import { Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-actividades',
  templateUrl: './modal-actividades.page.html',
  styleUrls: ['./modal-actividades.page.scss'],
})
export class ModalActividadesPage implements OnInit {
@Input()
urlSafe: SafeResourceUrl;

  constructor(private modalController: ModalController, public sanitizer: DomSanitizer) { }
  passurl: string;
  ngOnInit() {
    this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(this.passurl);
    console.log(this.urlSafe);
  }

}
