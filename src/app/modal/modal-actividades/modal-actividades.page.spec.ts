import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalActividadesPage } from './modal-actividades.page';

describe('ModalActividadesPage', () => {
  let component: ModalActividadesPage;
  let fixture: ComponentFixture<ModalActividadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalActividadesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalActividadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
