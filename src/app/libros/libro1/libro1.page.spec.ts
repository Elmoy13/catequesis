import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Libro1Page } from './libro1.page';

describe('Libro1Page', () => {
  let component: Libro1Page;
  let fixture: ComponentFixture<Libro1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Libro1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Libro1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
