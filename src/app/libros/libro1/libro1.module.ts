import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Libro1PageRoutingModule } from './libro1-routing.module';

import { Libro1Page } from './libro1.page';
import { ModalActividadesPage } from 'src/app/modal/modal-actividades/modal-actividades.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Libro1PageRoutingModule
  ],
  declarations: [Libro1Page, ModalActividadesPage],
  entryComponents: [ModalActividadesPage]
})
export class Libro1PageModule {}
