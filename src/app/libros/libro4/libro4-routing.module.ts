import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Libro4Page } from './libro4.page';

const routes: Routes = [
  {
    path: '',
    component: Libro4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Libro4PageRoutingModule {}
