import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-libro4',
  templateUrl: './libro4.page.html',
  styleUrls: ['./libro4.page.scss'],
})
export class Libro4Page implements OnInit {
 
  data: any;
  arrayAct:any[]=[
    {"img":"../../assets/Imagenes/actividad4-1.PNG","descripcion":"Actividad 1","titulo":"Tema 1"},
    {"img":"../../assets/Imagenes/actividad4-2.PNG","descripcion":"Actividad 2","titulo":"Tema 2"},
    {"img":"../../assets/Imagenes/actividad4-3.PNG","descripcion":"Actividad 3","titulo":"Tema 3"},
    {"img":"../../assets/Imagenes/actividad4-4.PNG","descripcion":"Actividad 4","titulo":"Tema 4"},
    {"img":"../../assets/Imagenes/actividad4-5.PNG","descripcion":"Actividad 5","titulo":"Tema 5"},
    {"img":"../../assets/Imagenes/actividad4-6.PNG","descripcion":"Actividad 6","titulo":"Tema 6"},
    {"img":"../../assets/Imagenes/actividad4-7.PNG","descripcion":"Actividad 7","titulo":"Tema 7"},
    {"img":"../../assets/Imagenes/actividad4-8.PNG","descripcion":"Actividad 8","titulo":"Tema 8"},
    {"img":"../../assets/Imagenes/actividad4-9.PNG","descripcion":"Actividad 9","titulo":"Tema 9"},
    {"img":"../../assets/Imagenes/actividad4-10.PNG","descripcion":"Actividad 10","titulo":"Tema 10"},
    {"img":"../../assets/Imagenes/actividad4-11.PNG","descripcion":"Actividad 11","titulo":"Tema 11"},
    {"img":"../../assets/Imagenes/actividad4-12.PNG","descripcion":"Actividad 12","titulo":"Tema 12"},
    {"img":"../../assets/Imagenes/actividad4-13.PNG","descripcion":"Actividad 13","titulo":"Tema 13"},
    {"img":"../../assets/Imagenes/actividad4-14.PNG","descripcion":"Actividad 14","titulo":"Tema 14"},
    {"img":"../../assets/Imagenes/actividad4-15.PNG","descripcion":"Actividad 15","titulo":"Tema 15"},
    {"img":"../../assets/Imagenes/actividad4-16.PNG","descripcion":"Actividad 16","titulo":"Tema 16"},
    {"img":"../../assets/Imagenes/actividad4-17.PNG","descripcion":"Actividad 17","titulo":"Tema 17"},
    {"img":"../../assets/Imagenes/actividad4-18.PNG","descripcion":"Actividad 18","titulo":"Tema 18"},
    {"img":"../../assets/Imagenes/actividad4-19.PNG","descripcion":"Actividad 19","titulo":"Tema 19"},
    {"img":"../../assets/Imagenes/actividad4-20.PNG","descripcion":"Actividad 20","titulo":"Tema 20"},
    {"img":"../../assets/Imagenes/actividad4-21.PNG","descripcion":"Actividad 21","titulo":"Tema 21"},
    {"img":"../../assets/Imagenes/actividad4-22.PNG","descripcion":"Actividad 22","titulo":"Tema 22"},
    {"img":"../../assets/Imagenes/actividad4-23.PNG","descripcion":"Actividad 23","titulo":"Tema 23"},
    {"img":"../../assets/Imagenes/actividad4-24.PNG","descripcion":"Actividad 24","titulo":"Tema 24"},
    {"img":"../../assets/Imagenes/actividad4-25.PNG","descripcion":"Actividad 25","titulo":"Tema 25"},
    {"img":"../../assets/Imagenes/actividad4-26.PNG","descripcion":"Actividad 26","titulo":"Tema 26"},
    {"img":"../../assets/Imagenes/actividad4-27.PNG","descripcion":"Actividad 27","titulo":"Tema 27"},
    {"img":"../../assets/Imagenes/actividad4-28.PNG","descripcion":"Actividad 28","titulo":"Tema 28"},
    {"img":"../../assets/Imagenes/actividad4-29.PNG","descripcion":"Actividad 29","titulo":"Tema 29"},
    {"img":"../../assets/Imagenes/actividad4-30.PNG","descripcion":"Actividad 30","titulo":"Tema 30"},
    {"img":"../../assets/Imagenes/actividad4-31.PNG","descripcion":"Actividad 31","titulo":"Tema 31"},
    {"img":"../../assets/Imagenes/actividad4-32.PNG","descripcion":"Actividad 32","titulo":"Tema 32"},
    {"img":"../../assets/Imagenes/actividad4-33.PNG","descripcion":"Actividad 33","titulo":"Tema 33"},
    {"img":"../../assets/Imagenes/actividad4-34.PNG","descripcion":"Actividad 34","titulo":"Tema 34"},
    {"img":"../../assets/Imagenes/actividad4-35.PNG","descripcion":"Actividad 35","titulo":"Tema 35"},
    {"img":"../../assets/Imagenes/actividad4-36.PNG","descripcion":"Actividad 36","titulo":"Tema 36"},
    {"img":"../../assets/Imagenes/actividad4-37.PNG","descripcion":"Actividad 37","titulo":"Tema 37"},
    {"img":"../../assets/Imagenes/actividad4-38.PNG","descripcion":"Actividad 38","titulo":"Tema 38"},
    {"img":"../../assets/Imagenes/actividad4-39.PNG","descripcion":"Actividad 39","titulo":"Tema 39"},
    {"img":"../../assets/Imagenes/actividad4-40.PNG","descripcion":"Actividad 40","titulo":"Tema 40"},
    {"img":"../../assets/Imagenes/actividad4-41.PNG","descripcion":"Actividad 41","titulo":"Tema 41"},
    {"img":"../../assets/Imagenes/actividad4-42.PNG","descripcion":"Actividad 42","titulo":"Tema 42"},
    {"img":"../../assets/Imagenes/actividad4-43.PNG","descripcion":"Actividad 43","titulo":"Tema 43"},
    {"img":"../../assets/Imagenes/actividad4-44.PNG","descripcion":"Actividad 44","titulo":"Tema 44"},
    {"img":"../../assets/Imagenes/actividad4-45.PNG","descripcion":"Actividad 45","titulo":"Tema 45"},
    {"img":"../../assets/Imagenes/actividad4-46.PNG","descripcion":"Actividad 46","titulo":"Tema 46"},
    {"img":"../../assets/Imagenes/actividad4-47.PNG","descripcion":"Actividad 47","titulo":"Tema 47"},
    {"img":"../../assets/Imagenes/actividad4-48.PNG","descripcion":"Actividad 48","titulo":"Tema 48"},
    {"img":"../../assets/Imagenes/actividad4-49.PNG","descripcion":"Actividad 49","titulo":"Tema 49"},
    {"img":"../../assets/Imagenes/actividad4-50.PNG","descripcion":"Actividad 50","titulo":"Tema 50"},
    {"img":"../../assets/Imagenes/actividad4-51.PNG","descripcion":"Actividad 51","titulo":"Tema 51"},
    {"img":"../../assets/Imagenes/actividad4-52.PNG","descripcion":"Actividad 52","titulo":"Tema 52"},
    {"img":"../../assets/Imagenes/actividad4-53.PNG","descripcion":"Actividad 53","titulo":"Tema 53"},
    {"img":"../../assets/Imagenes/actividad4-54.PNG","descripcion":"Actividad 54","titulo":"Tema 54"},
    {"img":"../../assets/Imagenes/actividad4-55.PNG","descripcion":"Actividad 55","titulo":"Tema 55"},
    {"img":"../../assets/Imagenes/actividad4-56.PNG","descripcion":"Actividad 56","titulo":"Tema 56"}
]
  constructor() {
    console.log(this.arrayAct)
   }

  

  ngOnInit() {}

}
