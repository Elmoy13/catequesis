import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Libro4Page } from './libro4.page';

describe('Libro4Page', () => {
  let component: Libro4Page;
  let fixture: ComponentFixture<Libro4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Libro4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Libro4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
