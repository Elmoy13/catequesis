import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Libro4PageRoutingModule } from './libro4-routing.module';

import { Libro4Page } from './libro4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Libro4PageRoutingModule
  ],
  declarations: [Libro4Page]
})
export class Libro4PageModule {}
