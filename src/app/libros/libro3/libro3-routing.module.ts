import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Libro3Page } from './libro3.page';

const routes: Routes = [
  {
    path: '',
    component: Libro3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Libro3PageRoutingModule {}
