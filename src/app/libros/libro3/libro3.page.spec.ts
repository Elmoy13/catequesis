import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Libro3Page } from './libro3.page';

describe('Libro3Page', () => {
  let component: Libro3Page;
  let fixture: ComponentFixture<Libro3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Libro3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Libro3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
