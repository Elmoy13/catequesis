import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Libro3PageRoutingModule } from './libro3-routing.module';

import { Libro3Page } from './libro3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Libro3PageRoutingModule
  ],
  declarations: [Libro3Page]
})
export class Libro3PageModule {}
