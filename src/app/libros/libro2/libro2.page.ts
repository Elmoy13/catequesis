import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-libro2',
  templateUrl: './libro2.page.html',
  styleUrls: ['./libro2.page.scss'],
})
export class Libro2Page implements OnInit {

  data: any;
  arrayAct:any[]=[
    {"img":"../../assets/Imagenes/actividad2-1.PNG","descripcion":"Actividad 1","titulo":"Tema 1"},
    {"img":"../../assets/Imagenes/actividad2-2.PNG","descripcion":"Actividad 2","titulo":"Tema 2"},
    {"img":"../../assets/Imagenes/actividad2-3.PNG","descripcion":"Actividad 3","titulo":"Tema 3"},
    {"img":"../../assets/Imagenes/actividad2-4.PNG","descripcion":"Actividad 4","titulo":"Tema 4"},
    {"img":"../../assets/Imagenes/actividad2-5.PNG","descripcion":"Actividad 5","titulo":"Tema 5"},
    {"img":"../../assets/Imagenes/actividad2-6.PNG","descripcion":"Actividad 6","titulo":"Tema 6"},
    {"img":"../../assets/Imagenes/actividad2-7.PNG","descripcion":"Actividad 7","titulo":"Tema 7"},
    {"img":"../../assets/Imagenes/actividad2-8.PNG","descripcion":"Actividad 8","titulo":"Tema 8"},
    {"img":"../../assets/Imagenes/actividad2-9.PNG","descripcion":"Actividad 9","titulo":"Tema 9"},
    {"img":"../../assets/Imagenes/actividad2-10.PNG","descripcion":"Actividad 10","titulo":"Tema 10"},
    {"img":"../../assets/Imagenes/actividad2-11.PNG","descripcion":"Actividad 11","titulo":"Tema 11"},
    {"img":"../../assets/Imagenes/actividad2-12.PNG","descripcion":"Actividad 12","titulo":"Tema 12"},
    {"img":"../../assets/Imagenes/actividad2-13.PNG","descripcion":"Actividad 13","titulo":"Tema 13"},
    {"img":"../../assets/Imagenes/actividad2-14.PNG","descripcion":"Actividad 14","titulo":"Tema 14"},
    {"img":"../../assets/Imagenes/actividad2-15.PNG","descripcion":"Actividad 15","titulo":"Tema 15"},
    {"img":"../../assets/Imagenes/actividad2-16.PNG","descripcion":"Actividad 16","titulo":"Tema 16"},
    {"img":"../../assets/Imagenes/actividad2-17.PNG","descripcion":"Actividad 17","titulo":"Tema 17"},
    {"img":"../../assets/Imagenes/actividad2-18.PNG","descripcion":"Actividad 18","titulo":"Tema 18"},
    {"img":"../../assets/Imagenes/actividad2-19.PNG","descripcion":"Actividad 19","titulo":"Tema 19"},
    {"img":"../../assets/Imagenes/actividad2-20.PNG","descripcion":"Actividad 20","titulo":"Tema 20"},
    {"img":"../../assets/Imagenes/actividad2-21.PNG","descripcion":"Actividad 21","titulo":"Tema 21"},
    {"img":"../../assets/Imagenes/actividad2-22.PNG","descripcion":"Actividad 22","titulo":"Tema 22"},
    {"img":"../../assets/Imagenes/actividad2-23.PNG","descripcion":"Actividad 23","titulo":"Tema 23"},
    {"img":"../../assets/Imagenes/actividad2-24.PNG","descripcion":"Actividad 24","titulo":"Tema 24"},
    {"img":"../../assets/Imagenes/actividad2-25.PNG","descripcion":"Actividad 25","titulo":"Tema 25"},
    {"img":"../../assets/Imagenes/actividad2-26.PNG","descripcion":"Actividad 26","titulo":"Tema 26"},
    {"img":"../../assets/Imagenes/actividad2-27.PNG","descripcion":"Actividad 27","titulo":"Tema 27"},
    {"img":"../../assets/Imagenes/actividad2-28.PNG","descripcion":"Actividad 28","titulo":"Tema 28"},
    {"img":"../../assets/Imagenes/actividad2-29.PNG","descripcion":"Actividad 29","titulo":"Tema 29"},
    {"img":"../../assets/Imagenes/actividad2-30.PNG","descripcion":"Actividad 30","titulo":"Tema 30"},
    {"img":"../../assets/Imagenes/actividad2-31.PNG","descripcion":"Actividad 31","titulo":"Tema 31"},
    {"img":"../../assets/Imagenes/actividad2-32.PNG","descripcion":"Actividad 32","titulo":"Tema 32"},
    {"img":"../../assets/Imagenes/actividad2-33.PNG","descripcion":"Actividad 33","titulo":"Tema 33"},
    {"img":"../../assets/Imagenes/actividad2-34.PNG","descripcion":"Actividad 34","titulo":"Tema 34"},
    {"img":"../../assets/Imagenes/actividad2-35.PNG","descripcion":"Actividad 35","titulo":"Tema 35"},
    {"img":"../../assets/Imagenes/actividad2-36.PNG","descripcion":"Actividad 36","titulo":"Tema 36"},
    {"img":"../../assets/Imagenes/actividad2-37.PNG","descripcion":"Actividad 37","titulo":"Tema 37"},
    {"img":"../../assets/Imagenes/actividad2-38.PNG","descripcion":"Actividad 38","titulo":"Tema 38"},
    {"img":"../../assets/Imagenes/actividad2-39.PNG","descripcion":"Actividad 39","titulo":"Tema 39"}
]
  constructor() {
    console.log(this.arrayAct)
   }

  

  

  ngOnInit() {}

}