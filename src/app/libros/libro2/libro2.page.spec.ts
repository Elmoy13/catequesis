import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Libro2Page } from './libro2.page';

describe('Libro2Page', () => {
  let component: Libro2Page;
  let fixture: ComponentFixture<Libro2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Libro2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Libro2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
