import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Libro2PageRoutingModule } from './libro2-routing.module';

import { Libro2Page } from './libro2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Libro2PageRoutingModule
  ],
  declarations: [Libro2Page]
})
export class Libro2PageModule {}
