import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Libro2Page } from './libro2.page';

const routes: Routes = [
  {
    path: '',
    component: Libro2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Libro2PageRoutingModule {}
